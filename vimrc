set nocompatible
filetype plugin on
syntax on
filetype plugin indent on
"let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"

"On est pas pressé
"set timeoutlen=3000

" Pour split window
map <Tab> <C-W>w
map <Bar> <C-W>v<C-W><Right>
map -     <C-W>s<C-W><Down>
nnoremap gb :ls<CR>:b<Space>
" les F
nmap <F1> :set mouse=a<ENTER>
nmap <F2> :set mouse=<ENTER>
nmap <F3> :NERDTreeFocus<ENTER>
nmap <F5> :source ~/.vimrc<ENTER>
nmap <F8> :tabn<ENTER>
nmap <F7> :tabp<ENTER>
nmap <F10> :set nu!<ENTER>
nmap <F12> :! /5g/tools/clang/clang-4.0/bin/clang-format -i %<ENTER>L<ENTER>:e<ENTER>

nnoremap <PageUp> <PageUp>zz
nnoremap <PageDown> <PageDown>zz
nnoremap gf <C-W>gf

set nu
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
set formatoptions-=cro
set expandtab
set shiftwidth=4
set smarttab
set autoindent
set showcmd
set hlsearch
set listchars=tab:>-
" pas de touche fleché :)
"no <down> <Nop>
"no <up> <Nop>
"no <left> <Nop>
"no <right> <Nop>
"vno <down> <Nop>
"vno <up> <Nop>
"vno <left> <Nop>
"vno <right> <Nop>

" on replis
" set foldmethod=syntax
au BufRead * normal zR

" Pour virer le surlignement
nnoremap <c-l> <c-l>:noh<cr>

" Le C++ facile
autocmd FileType cpp,c,cxx,h,hpp inoremap ;; <Esc>/<++><Enter>"_c4l
autocmd FileType cpp,c,cxx,h,hpp inoremap ;tem <C-G>utemplate<class ><Enter><++><Esc>k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;uns <C-G>uusing namespace std;<Esc>i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;i< <C-G>u#include <><Esc>i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;pr <C-G>u#pragma once<Enter>
autocmd FileType cpp,c,cxx,h,hpp inoremap ;i" <C-G>u#include ""<Esc>i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;ie <C-G>uif()<Enter>{<Enter><++><Enter>}<Enter>else<Enter>{<Enter><++><Enter>}<Esc>7k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;if <C-G>uif()<Enter>{<Enter><++><Enter>}<Esc>3k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;w <C-G>uwhile()<Enter>{<Enter><++><Enter>}<Esc>3k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;pa <C-G>u()<Enter>{<Enter><++><Enter>}<Enter><Esc>3k$F(a
autocmd FileType cpp,c,cxx,h,hpp inoremap ;for <C-G>ufor()<Enter>{<Enter><++><Enter>}<Esc>3k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;cl <C-G>uclass  <Enter>{<Enter><++><Enter>};<Esc>3k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;str <C-G>ustruct  <Enter>{<Enter><++><Enter>};<Esc>3k$i
autocmd FileType cpp,c,cxx,h,hpp inoremap ;t32 <C-G>uuint32_t 
autocmd FileType cpp,c,cxx,h,hpp inoremap ;t_ <C-G>uuint8_t 
autocmd FileType cpp,c,cxx,h,hpp inoremap ;un <C-G>uunsigned 
autocmd FileType cpp,c,cxx,h,hpp inoremap ;st <C-G>ustatic 
autocmd FileType cpp,c,cxx,h,hpp inoremap ;sa <C-G>ustatic_assert( 
autocmd FileType cpp,c,cxx,h,hpp inoremap ;co <C-G>uconstexpr 


