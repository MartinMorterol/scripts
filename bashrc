
source $( dirname "${BASH_SOURCE[0]}" )/bashrc_bach
source $( dirname "${BASH_SOURCE[0]}" )/bashrc_cpp
source $( dirname "${BASH_SOURCE[0]}" )/bashrc_git


PS1='[\[\033[1;35m\]$(uname -n)\[\033[00m\]]\[\033[1;32m\]\u\[\033[00m\]:\[\033[1;34m\]${PWD#"${PWD%/*/*}/"}\[\033[1;31m\]$(name_branch_no_git)\[\033[00m\]\$ '
PROMPT_COMMAND='echo -ne "\033]0;[$(uname -n)] $(name_branch_no_git) ${PWD#"${PWD%/*}/"} \007"'


